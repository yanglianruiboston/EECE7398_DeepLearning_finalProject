import matplotlib.pyplot as plt
from tensorflow.keras.utils import to_categorical
from models import *
from utils import *

def GAN_classify(batch_size,epochs,discriminator=None):
    '''
    The fonction to diagonse
    :param: batch_size:
            epochs:
            discriminator: network for classify. default is none, which means untrained discriminator
    '''
    if discriminator == None:
        discriminator=load_discriminator()
    classify_model = Classify(discriminator)
    classify_model.summary()

    (train_imgs, train_lb),(test_imgs, test_lb) = load_data(batch_size)

    #print(train_imgs.shape, train_lb.shape, test_imgs.shape, test_lb.shape, '\n')

    train_labels = to_categorical(train_lb)
    test_labels = to_categorical(test_lb)

    classify_model.compile(optimizer='sgd',
                           loss='categorical_crossentropy',
                           metrics=['accuracy'])

    history = classify_model.fit(train_imgs, train_labels, batch_size=batch_size, epochs=epochs,
                            validation_data=(test_imgs, test_labels))
    classify_model.save('./training_checkpoints/GANclf.h5')
    plt.figure()
    plt.plot(history.history['accuracy'], label='accuracy')
    plt.plot(history.history['val_accuracy'], label='val_accuracy')
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.ylim([0.6, 1.01])
    plt.legend(loc='lower right')
    plt.savefig('./result_pics/acc_with_trained_Discriminator')

    plt.figure()
    plt.plot(history.history['loss'], label='loss')
    plt.plot(history.history['val_loss'], label='val_loss')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.legend(loc='lower right')
    plt.savefig('./result_pics/loss_with_trained_Discriminator')
    #plt.show()


