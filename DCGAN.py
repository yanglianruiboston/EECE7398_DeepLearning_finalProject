from utils import *
from models import Generator,Discriminator
from distill import distill_network
from GANclassify import GAN_classify
import os
import time

def train_gan(batch_size,epochs,withDIS,classify_epoch):
    '''
    The fonction to train gan (generator and discriminator) and distill student network
    :param: batch_size: batch_size for dataset
             epochs: training epochs for gan
             withDIS: apply distillation or not
             classify_epoch: epoches for training classifiers or distillation
    '''
    (train_imgs, train_lb),(_,_) = load_data(batch_size)

    print(train_imgs.shape, train_lb.shape, '\n')

    generator = Generator()
    discriminator = Discriminator()

    # This method returns a helper function to compute cross entropy loss
    cross_entropy = tf.keras.losses.BinaryCrossentropy(from_logits=True)

    generator_optimizer = tf.keras.optimizers.Adam(0.0001)
    discriminator_optimizer = tf.keras.optimizers.Adam(0.0001)

    checkpoint_dir = './training_checkpoints/GAN/'
    checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt")
    checkpoint = tf.train.Checkpoint(generator_optimizer=generator_optimizer,
                                     discriminator_optimizer=discriminator_optimizer,
                                     generator=generator,
                                     discriminator=discriminator)
    #checkpoint = checkpoint.restore(tf.train.latest_checkpoint(checkpoint_dir))

    # train
    for epoch in range(epochs):
        start = time.time()

        for image_batch in train_imgs:
            image_batch=image_batch.reshape([1,512,512,3])
            train_step(image_batch,batch_size,cross_entropy,generator,discriminator)

        # Save the model every 5 epochs
        if (epoch+1) % 20 == 0:
            checkpoint.save(file_prefix=checkpoint_prefix)
        print ('Time for epoch {} is {} sec'.format(epoch + 1, time.time()-start))
    discriminator.save(checkpoint_dir+'GAN.h5')

    if withDIS:
        distill_network(batch_size,classify_epoch,discriminator)
    else:
        GAN_classify(batch_size,classify_epoch,discriminator)