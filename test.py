from PIL import Image
from models import *
from utils import *

def test(img_path,withDIS=True):
    '''
    The fonction to diagonse
    :param: img_path: path for the image
             withDIS: apply distillation or not,default is True
    '''
    img = Image.open(img_path)
    img = img.resize((512, 512))
    img = img.convert("RGB")
    img = np.array(img) / 255.
    img = (np.expand_dims(img, 0))

    if withDIS == True:
        teacher = Discriminator()
        student = StudentDiscriminator()
        classify_model = Distiller(student=student, teacher=teacher)
        classify_model.load_weights('./training_checkpoints/DISTILL/distill').expect_partial()
    else:
        if os.path.exists('./training_checkpoints/GANclf.h5'):
            classify_model = keras.models.load_model('./training_checkpoints/GANclf.h5')
        else:
            discriminator=Discriminator()
            classify_model = Classify(discriminator)

    prediction = classify_model.predict(img)
    prediction = np.argmax(prediction)
    labels=['Covid','Normal','Viral Pneumonia']
    print('prediction:', labels[prediction])