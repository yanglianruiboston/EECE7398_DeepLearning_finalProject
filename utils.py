from tensorflow.keras.preprocessing.image import ImageDataGenerator
import tensorflow as tf
from models import Discriminator
from tensorflow import keras
import os
from math import ceil
import numpy as np
import argparse

# generate data in dataset
def gen_data(total,batch_size,data_gen):
    x=[]
    y=[]
    for j in range(ceil(total/batch_size)):
        x_batch, y_batch = data_gen.next()
        y_batch=y_batch.tolist()
        for i in range(len(x_batch)):
             x.append(x_batch[i])
             y.append(y_batch[i].index(float(1)))
    return np.array(x),np.array(y)

# load dataset for training
def load_data(batch_size):
    image_path = "./dataset"
    train_dir = image_path + "/train"
    test_dir = image_path + "/test"
    img_size=(512,512)

    train_image_generator = ImageDataGenerator( rescale=1./255,
                                                shear_range=0.2,
                                                zoom_range=0.2,
                                                horizontal_flip=True)
    test_image_generator = ImageDataGenerator(rescale=1./255)

    train_data_gen = train_image_generator.flow_from_directory(directory=train_dir,
                                                               batch_size=batch_size,
                                                               shuffle=True,
                                                               target_size=img_size,
                                                               class_mode='categorical')

    total_train = train_data_gen.n

    test_data_gen = test_image_generator.flow_from_directory(directory=test_dir,
                                                             batch_size=batch_size,
                                                             shuffle=True,
                                                             target_size=img_size,
                                                             class_mode='categorical')

    total_test = test_data_gen.n

    train_imgs, train_lb = gen_data(total_train, batch_size, train_data_gen)
    test_imgs, test_lb = gen_data(total_test, batch_size, test_data_gen)

    return (train_imgs,train_lb),(test_imgs,test_lb)

# covert input string to bool value.
def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 'True','t', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'False','f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def load_discriminator():
    checkpoint_dir = './training_checkpoints/GAN/'
    if os.path.exists(checkpoint_dir + 'GAN.h5'):
        discriminator = keras.models.load_model(checkpoint_dir + 'GAN.h5')
    else:
        discriminator = Discriminator()
    return discriminator

def discriminator_loss(cross_entropy,real_output, fake_output):
    real_loss = cross_entropy(tf.ones_like(real_output), real_output)
    fake_loss = cross_entropy(tf.zeros_like(fake_output), fake_output)
    total_loss = real_loss + fake_loss
    return total_loss

def generator_loss(cross_entropy,fake_output):
    return cross_entropy(tf.ones_like(fake_output), fake_output)

generator_optimizer = tf.keras.optimizers.Adam(0.0001)
discriminator_optimizer = tf.keras.optimizers.Adam(0.0001)

# Notice the use of `tf.function`
# This annotation causes the function to be "compiled".
@tf.function
def train_step(images,batch_size,cross_entropy,generator,discriminator,noise_dim=100):
    noise = tf.random.normal([batch_size, noise_dim])
    with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
      generated_images = generator(noise, training=True)

      real_output = discriminator(images, training=True)
      fake_output = discriminator(generated_images, training=True)

      gen_loss = generator_loss(cross_entropy,fake_output)
      disc_loss = discriminator_loss(cross_entropy,real_output, fake_output)

    gradients_of_generator = gen_tape.gradient(gen_loss, generator.trainable_variables)
    gradients_of_discriminator = disc_tape.gradient(disc_loss, discriminator.trainable_variables)

    generator_optimizer.apply_gradients(zip(gradients_of_generator, generator.trainable_variables))
    discriminator_optimizer.apply_gradients(zip(gradients_of_discriminator, discriminator.trainable_variables))
