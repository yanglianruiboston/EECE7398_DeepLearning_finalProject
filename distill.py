from models import *
from utils import *
import matplotlib.pyplot as plt

##########################################################################################################
def distill_network(batchSize,DistillEpoch,teacher=None):
    '''
    The fonction to distill teacher network
    :param: batchSize:
            DistillEpoch: epochs for distillation
            teacher: teacher model. default is none, which means untrained teacher network.
    '''
    if teacher == None:
        teacher = load_discriminator()
    student = StudentDiscriminator()

    #train img  train label     test img   test label
    (x_train,     y_train),      (x_test,     y_test) = load_data(batchSize)

    distiller = Distiller(student=student, teacher=teacher)
    distiller.student.summary()
    distiller.compile(
        optimizer=keras.optimizers.Adam(),
        metrics=[keras.metrics.SparseCategoricalAccuracy()],
        student_loss_fn=keras.losses.SparseCategoricalCrossentropy(from_logits=True),
        distillation_loss_fn=keras.losses.KLDivergence(),
        alpha=0.2,
        temperature=2,
        )

    # Distill teacher to student
    history=distiller.fit(x_train, y_train, epochs=DistillEpoch, batch_size=batchSize,
                  validation_data=(x_test, y_test))

    # Evaluate student on test dataset
    #distiller.evaluate(x_test, y_test)

    distiller.save_weights('./training_checkpoints/DISTILL/distill')
    plt.figure()
    plt.plot(history.history['sparse_categorical_accuracy'], label='accuracy')
    plt.plot(history.history['val_sparse_categorical_accuracy'], label='val_accuracy')
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.ylim([0.6, 1.01])
    plt.legend(loc='lower right')
    plt.savefig('./result_pics/acc_with_distillation')

    plt.figure()
    plt.plot(history.history['student_loss'], label='loss')
    plt.plot(history.history['val_student_loss'], label='val_loss')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.legend(loc='lower right')
    plt.savefig('./result_pics/loss_with_distillation')
    #plt.show()
############################################################################################################
