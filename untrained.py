import matplotlib.pyplot as plt
from tensorflow.keras.utils import to_categorical
from models import *
from utils import *

'''
For the results without pretrained GAN
'''

def distill_network(batchSize,DistillEpoch,teacher=None):
    if teacher == None:
        teacher = load_discriminator()
    student = StudentDiscriminator()

    #train img  train label     test img   test label
    (x_train,     y_train),      (x_test,     y_test) = load_data(batchSize)# 读取covid x光片的 dataset

    distiller = Distiller(student=student, teacher=teacher)
    distiller.student.summary()
    distiller.compile(
        optimizer=keras.optimizers.Adam(),
        metrics=[keras.metrics.SparseCategoricalAccuracy()],
        student_loss_fn=keras.losses.SparseCategoricalCrossentropy(from_logits=True),
        distillation_loss_fn=keras.losses.KLDivergence(),
        alpha=0.5,
        temperature=5,
        )

    # Distill teacher to student
    history=distiller.fit(x_train, y_train, epochs=DistillEpoch, batch_size=batchSize,
                  validation_data=(x_test, y_test))

    # Evaluate student on test dataset
    #distiller.evaluate(x_test, y_test)

    distiller.save_weights('./training_checkpoints/DISTILL/distill')
    plt.figure()
    plt.plot(history.history['sparse_categorical_accuracy'], label='accuracy')
    plt.plot(history.history['val_sparse_categorical_accuracy'], label='val_accuracy')
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.ylim([0.6, 1.01])
    plt.legend(loc='lower right')
    plt.savefig('./result_pics/acc_with_undistill_student')

    plt.figure()
    plt.plot(history.history['student_loss'], label='loss')
    plt.plot(history.history['val_student_loss'], label='val_loss')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    #plt.ylim([0.6, 1.01])
    plt.legend(loc='lower right')
    plt.savefig('./result_pics/loss_with_undistill_student')
    #plt.show()
############################################################################################################



def GAN_classify(batch_size,epochs,discriminator=None):
    if discriminator == None:
        discriminator=load_discriminator()
    classify_model = Classify(discriminator)
    classify_model.summary()

    (train_imgs, train_lb),(test_imgs, test_lb) = load_data(batch_size)

    #print(train_imgs.shape, train_lb.shape, test_imgs.shape, test_lb.shape, '\n')

    train_labels = to_categorical(train_lb)
    test_labels = to_categorical(test_lb)

    classify_model.compile(optimizer='sgd',
                           loss='categorical_crossentropy',
                           metrics=['accuracy'])

    history = classify_model.fit(train_imgs, train_labels, batch_size=batch_size, epochs=epochs,
                            validation_data=(test_imgs, test_labels))
    classify_model.save('./training_checkpoints/GANclf.h5')
    plt.figure()
    plt.plot(history.history['accuracy'], label='accuracy')
    plt.plot(history.history['val_accuracy'], label='val_accuracy')
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.ylim([0.6, 1.01])
    plt.legend(loc='lower right')
    plt.savefig('./result_pics/acc_with_untrained_GAN')

    plt.figure()
    plt.plot(history.history['loss'], label='loss')
    plt.plot(history.history['val_loss'], label='val_loss')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    #plt.ylim([0.6, 1.01])
    plt.legend(loc='lower right')
    plt.savefig('./result_pics/loss_with_untrained_GAN')
    #plt.show()

#执行distill_network函数
distill_network(batchSize=8 , DistillEpoch=50) # batchsize 应该和训练gan时的batchsize一致
GAN_classify(8,50)

