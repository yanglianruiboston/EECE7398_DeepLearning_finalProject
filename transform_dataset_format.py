import os

def get_filelist(dir, Filelist):
    '''
    list all files in the dir
    :param: dir: path of the directory
            Filelist: empty list([])
    :return: Filelist: All files' path in the input dir
    '''
    if os.path.isfile(dir):
        Filelist.append(dir)
    elif os.path.isdir(dir):
        for s in os.listdir(dir):
            newDir = os.path.join(dir, s)
            get_filelist(newDir, Filelist)
    return Filelist

def rename(path):
    '''
    rename all files to .jpeg in the path
    :param: path: path of dataset
    '''
    filelist = get_filelist(path, [])
    temp=''
    a=1
    for file in filelist:
        new_path=os.path.split(file)[0]
        file_prefix=os.path.split(new_path)[1]
        file_prefix=os.path.splitext(file_prefix)[0]
        if temp != file_prefix:
            a=1
            temp=file_prefix
        Newdir = os.path.join(new_path, file_prefix +'-'+str(a)+ '.jpeg')
        a+=1
        os.rename(file, Newdir)
        print(file+'-->'+Newdir)



if __name__ == '__main__':
    rename('./dataset')


