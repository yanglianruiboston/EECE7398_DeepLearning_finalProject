# DCGAN and DISTILLATION for covid detection
## Author

[Yinan Wang](https://github.com/wyn0325)  
[Lianrui Yang](https://github.com/YangLianrui)

## Requirements

- Python 3.6+
- tensorflow-gpu==2.5.0
- h5py==2.10
- matplotlib
- numpy

## Dataset Composition

X-ray images come from:

    https://www.kaggle.com/pranavraikokte/covid19-image-dataset
    https://www.kaggle.com/paultimothymooney/chest-xray-pneumonia
    https://github.com/ieee8023/covid-chestxray-dataset

Train set: 296×3=888  
Test set:70×3=210

## Train or Test
To train with DISTILLATION:

    python main.py

To train without DISTILLATION:

    python main.py -withDIS False

To test with DISTILLATION:

    python main.py -mode test

To test without DISTILLATION:

    python main.py -mode test -withDIS False

**Change dataset :**

Edit dataset path in utils.py:load_data

**All params**

    $ -mode train/test DEFAULT:train
    $ -batch_size DEFAULT:8
    $ -gan_epoch DEFAULT:100
    $ -classify_epoch DEFAULT:100
    $ -withDIS DEFAULT:True

## Net Layers

![GAN discriminator](./layers_pics/Layers_GANclf.png)

![Distill](./layers_pics/Layers_distillation.png)

## Results
**Environments:**  
`   win+GTX1050TI`  
`   linux+K80`  
`   Cuda11.0 with Cudann`  

![acc with dis and GAN](./result_pics/acc_with_distillation.png)

![acc untrained student](./result_pics/acc_with_undistill_student.png)

![acc trained GAN](./result_pics/acc_with_trained_Discriminator.png)

![acc untrained GAN](./result_pics/acc_with_untrained_GAN.png)



![loss with dis and GAN](./result_pics/loss_with_distillation.png)

![loss untrained student](./result_pics/loss_with_undistill_student.png)

![loss trained GAN](./result_pics/loss_with_trained_Discriminator.png)

![loss untrained GAN](./result_pics/loss_with_untrained_GAN.png)



## py files detail

- main.py
- utils.py: functions except all train and test func
- models.py: all models(modified DCGAN,student for distillation)
- DCGAN.py: train the modified DCGAN net
- distill.py: apply distillation for classify
- GANclassify.py: classify the imgs with only GAN
- test.py: the test function
- untrained.py: train the discriminator and student network without pretrained GAN
- transform_dataset_format.py: change all imgs in dataset to .jpeg and rename


## Reference code

- [GAN on image classification](https://github.com/ck44liu/gans-on-image-classification)
- [DCGAN](https://www.tensorflow.org/tutorials/generative/dcgan)
- [Knowledge Distillation](https://keras.io/examples/vision/knowledge_distillation/)

