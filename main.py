import argparse
from DCGAN import train_gan
from test import test
from utils import str2bool
# Main
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # train/test
    parser.add_argument('-mode', type=str, default='train',help='Mode: train / test')
    # Batch_size
    parser.add_argument('-batch_size', type=int, default=8)
    # epoch for training gan
    parser.add_argument('-gan_epoch', type=int, default=100, help='number of epochs')
    # epoch for training classify network（with or without distillation）
    parser.add_argument('-classify_epoch', type=int, default=100)
    # with or not distillation
    parser.add_argument("-withDIS", type=str2bool,default=True, help='distillation or not')
    args = parser.parse_args()

    # train mode
    if args.mode == 'train':
        train_gan(args.batch_size,args.gan_epoch,args.withDIS,args.classify_epoch)
    # test mode
    elif args.mode == 'test':
        print("Input file path:", end=' ')
        img_path = str(input())
        test(img_path,args.withDIS)
